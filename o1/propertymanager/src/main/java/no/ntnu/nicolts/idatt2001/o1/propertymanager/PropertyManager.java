package no.ntnu.nicolts.idatt2001.o1.propertymanager;
import javax.swing.JOptionPane;

/**
 * PropertyManager has the responsibilty for the frontend functionality of the
 * application.
 * 
 * @author Nicolai Sivesind
 * @version 0.1
 */
public class PropertyManager {
    private static PropertyRegister gloppen;
    
    /** 
     * @param args
     */
    public static void main(String[] args){
        gloppen = new PropertyRegister("Gloppen", 1445);
        generateTestData();

        String[] mainMenu = { "Vis gjennomsnittlig eiendomsareal" , "Søk opp eiendom",
                              "Vis alle eiendommer", "Slett eiendom" ,"Registrer ny eiendom" };
        boolean ended = false;

        while (!ended) {
            int choice = multipleChoice("Hvilken handling vil du utføre", "Hovedmeny", mainMenu);
            switch (choice) {
                case 0 -> avgArea();
                case 1 -> searchForProperty();
                case 2 -> display("Alle eiendommer: \n\n" + gloppen.allProperties());
                case 3 -> removeProperty();
                case 4 -> registerProperty();
                default -> ended = true;
            }
        }

    }

    /**
     * Calculates and displays average area if possible. 
     */
    private static void avgArea(){
        try{
            display("Gjennomsnittlig eiendomsareal for " + gloppen.getPropertyList().size() + " eiendommer:\n"
                    + String.valueOf(gloppen.avgArea()));
        }catch(Exception e){
            display(e.toString());
        }
    }

    /**
     * Displayes a desired property, if it exists.  
     */
    private static void searchForProperty(){
        try {
            int kNr = Integer.parseInt(input("Kommunenummer:"));
            int gNr = Integer.parseInt(input("Gårdsnummer:"));
            int bNr = Integer.parseInt(input("Bruksnummer"));

            display("Gjennomsnitlig areal: " + gloppen.searchForProperty(kNr, gNr, bNr));
        } catch (Exception e) {
            display(e.toString());
        }
    }

    /**
     * Removes a disered property, if it exists.
     */
    private static void removeProperty(){
        int kNr = Integer.parseInt(input("Kommunenummer:"));
        int gNr = Integer.parseInt(input("Gårdsnummer:"));
        int bNr = Integer.parseInt(input("Bruksnummer"));

        try{
            gloppen.removeProperty(gloppen.searchForProperty(kNr, gNr, bNr));
            display("Fjernet eiendom.");
        } catch(Exception e) {
            display(e.toString());
        } 
    }

    /**
     * Registers informations from user to register a property.
     */
    private static void registerProperty(){
        String owner = input("Eier:");
        int gNr = Integer.parseInt(input("Gårdsnummer:"));
        int bNr = Integer.parseInt(input("Bruksnummer:"));
        String bName = input("Bruksnavn:");
        double area = Double.parseDouble(input("Eiendomsareal:"));

        try {
            gloppen.registerProperty(owner, gNr, bNr, bName, area);
        } catch (Exception e) {
            display("Det skjedde en feil. Eiendom ble ikke registrert. Feilmelding: " + e.toString());
        }
    }
    
    
    /**
     * Asks for userinput. 
     * Easier to change how user input is done on the whole client.
     * 
     * @param string
     * @return String
     */
    private static String input(String string) {
        return JOptionPane.showInputDialog(null, string);
    }
    
    
    /**
     * Displays a message. 
     * Easier to change how it is displayed on the whole klient.
     * @param string
     */
    private static void display(String string) {
        JOptionPane.showMessageDialog(null, string);
    }

    
    /** 
     * Dislpays a multiple choice menu based on an array of Objects.
     * @param message
     * @param title
     * @param choices
     * @return int
     */
    private static int multipleChoice(String message, String title, Object[] choices) {
        return JOptionPane.showOptionDialog(null, message, title, JOptionPane.DEFAULT_OPTION,
               JOptionPane.INFORMATION_MESSAGE, null, choices, choices[0]);
    }

    /**
     * Generates test data.
     */
    private static void generateTestData(){
        gloppen.registerProperty("Jens Olsen", 77, 631, 1017.16);
        gloppen.registerProperty("Nicolay Madsen", 77, 131, "Syningom", 661.3);
        gloppen.registerProperty("Evilyn Jensen", 75, 19, "Fugletun", 650.6);
        gloppen.registerProperty("Karl Ove Bråten", 74, 188, 1457.2);
        gloppen.registerProperty("Elsa Indregård", 69, 57, "Høiberg", 1339.4);
    }

    /*private static String listToString(ArrayList<Object> list){
        String output = "";
        for (Object object : list) output += object;
        return output;
    } */
}
