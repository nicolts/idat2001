package no.ntnu.nicolts.idatt2001.o1.propertymanager;

import java.util.ArrayList;

/**
 * PropertyRegisters has the resposibilty for the backend functionality of the
 * property application.
 * 
 * 3A Besvarelse: 
 * Jeg har valgt bruke av ArrayList pga. at det er enkelt å arbeide med den da 
 * den har en "ubegrenset" kapasitet. Vanlig array møter manpå problemet at man 
 * må definere kapasiteten til arrayen. Fordeler ved å evt. bruke vanlige arrays 
 * er at JVM behandler de fortere. Dette blir kun merkbart år man jobber med 
 * store mengder data. I tilfeller der man skal kun jobbe med en tabell i ett 
 * enkelt tilfelle, kan man også spare linjer med kode ved å bruke en vanlig 
 * array, da den er enklere å initialisere med verdier. 
 * En fordel med å bruke ArrayList er at den oppfører seg som (og er) en klasse, og tilbyr en
 * rekke metoder som gir et større bruksområde. I denne oppgaven skal vi legge til å
 * fjerne elementer i tabellen, og det er derfor hensiktsmessig å bruke en
 * ArrayList. Siden den implementerer Iteratable-interfacet er den kompatibel
 * med klassen Iterrator, som kan forenkle sletteprosessen.
 *  
 * @author Nicolai Sivesind
 * @version 0.1
 */
public class PropertyRegister {
    private ArrayList<Property> propertyList;
    private String kName;
    private int kNr;

    public PropertyRegister(String kName, int kNr) {
        propertyList = new ArrayList<>();
        this.kName = kName;
        this.kNr = kNr;
    }

    
    /**
     * Registers a property with bName.
     * 
     * @param owner
     * @param gNr
     * @param bNr
     * @param bName
     * @param area
     */
    public void registerProperty(String owner, int gNr, int bNr, String bName, double area){
        //sjekk unikhet
        propertyList.add(new Property(owner, this.kName, this.kNr, gNr, bNr, bName, area));
    }

    /** 
     * Registers a property without bName.
     * @param owner
     * @param gNr
     * @param bNr
     * @param area
     */
    public void registerProperty(String owner, int gNr, int bNr, double area) {
        propertyList.add(new Property(owner, this.kName, this.kNr, gNr, bNr, area));
    }

    /**
     * Removes a property from the register.
     * @param property takes in a Property to find a matching. 
     */
    public void removeProperty(Property property) {
        for(int i = 0; i < propertyList.size(); i++){
            if(propertyList.get(i).equals(property)){
                propertyList.remove(i);
                return;
            }
        }
    }

    
    /** 
     * @return All properties as a String.
     */
    public String allProperties(){
        String output = "";
        for (Property property : propertyList) {
            output += property + "\n";
        }
        return output;
    }

    public int amountOfProperties(){
        return propertyList.size();
    }

    
    /** 
     * Searches for a property based on kNr, gNR and bNr.
     * @param kNr
     * @param gNr
     * @param bNr
     * @return A property, if it matches all the parameters. 
     * @throws NullPointerException if it does not find a matching property.
     */
    public Property searchForProperty(int kNr, int gNr, int bNr) throws Exception{

        for (Property property : propertyList) {
            if(property.getKNr() == kNr && 
               property.getGNr() == gNr &&
               property.getBNr() == bNr) 
               return property; 
        }
        throw new Exception("Fant ikke eiendom.");
    }

    
    /** 
     * Calculates the average of all registered properties.
     * @return The average of all registered properties as double.
     * @throws NullPointerException if there is no registered properties.
     */
    public double avgArea() throws Exception {
        if(propertyList.size() == 0) throw new Exception("Det er ingen registrerte tomter");
        double totalArea = 0;
        
        for (Property property : propertyList) {
            totalArea += property.getArea();
        }

        return totalArea/propertyList.size();
    }

    /**
     * Finds all properties with matching gNr
     * @param gNr 
     * @return ArrayList of Propeties that has a matching gNr.
     */
    public ArrayList<Property> propertiesAt(int gNr){
        ArrayList<Property> matchingProperties = new ArrayList<>();
        for (Property property : propertyList) {
            if(property.getGNr() == gNr) matchingProperties.add(property);
        }
        return matchingProperties;
    }

    
    /** 
     * @return ArrayList<Property>
     */
    public ArrayList<Property> getPropertyList() {
        return this.propertyList;
    }

    
    /** 
     * @param propertyList
     */
    public void setPropertyList(ArrayList<Property> propertyList) {
        this.propertyList = propertyList;
    }
    
    /** 
     * @return String
     */
    public String getKName() {
        return this.kName;
    }

    
    /** 
     * @param kName
     */
    public void setKName(String kName) {
        this.kName = kName;
    }

    
    /** 
     * @return int
     */
    public int getKNr() {
        return this.kNr;
    }

    
    /** 
     * @param kNr
     */
    public void setKNr(int kNr) {
        this.kNr = kNr;
    }

    //toString

}
