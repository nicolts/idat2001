package no.ntnu.nicolts.idatt2001.o1.propertymanager;

/**
 * The class Property contains and processes the information of a single property.
 * 
 * Mutatormetoder-begrunnelse:
 * Oppgaven ber oss ikke om å kunne redigere informasjonen som blir lagt inn.
 * Det er derfor ikke et behov for mutatormetoder. Jeg har likevel inkludert det etter prinsippet
 * om cohesion. Det omhandler blant annet å bygge klasser uavhengige fra andre klasser, med mulighet for 
 * gjenbruk i andre klasser. Skulle det bli behov for å endre informasjonen (slik som feks. ved kommunesammenslåing
 * eller bytte av eier, evt. rivning, utvidning av tomten osv.) vil man slippe å refaktorere mindre som er et 
 * av kjennetegnene på høy cohesion.  
 *
 * @author Nicolai Sivesind
 * @version 0.1
 */
public class Property {
    private String owner;
    private String kName;
    private int kNr;
    private int gNr;
    private int bNr;
    private String bName;
    private double area;

    /**
     * One of two constructors of Property.
     * Initializes an object of the type Property with bNr.
     *
     * @param owner
     * @param kName
     * @param kNr
     * @param gNr
     * @param bNr
     * @param bName
     * @param area
     */
    public Property(String owner, String kName, int kNr, int gNr, int bNr, String bName, double area) {
        if (kNr < 101 || kNr > 5054) //legg til assert på negative tall
            throw new IllegalArgumentException("Kommune nr. er ugyldig.");
        this.owner = owner;
        this.kName = kName;
        this.kNr = kNr;
        this.gNr = gNr;
        this.bNr = bNr;
        this.bName = bName;
        this.area = area;
    }

    /**
     * One of two constructors of Propery.
     * Initializes an object of the type Property without bNr.
     * 
     * @param owner, String
     * @param kName
     * @param kNr, int between 101 and 5054
     * @param gNr
     * @param bNr
     * @param area
     * @throws IllegalArgumentException if kNr 
     */
    public Property(String owner, String kName, int kNr, int gNr, int bNr, double area) {
        if (kNr < 101 || kNr > 5054)
            throw new IllegalArgumentException("Kommune nr. er ugyldig.");
        this.owner = owner;
        this.kName = kName;
        this.kNr = kNr;
        this.gNr = gNr;
        this.bNr = bNr;
        this.bName = null;
        this.area = area;
    }

    /**
     * 
     * @return String owner
     */
    public String getOwner() {
        return this.owner;
    }

    
    /** 
     * @return String kName
     */
    public String getKName() {
        return this.kName;
    }

	public void setOwner(String owner) {
        this.owner = owner;
    }
    public void setKName(String kName) {
        this.kName = kName;
    }
    public void setKNr(int kNr) {
        this.kNr = kNr;
    }
    public void setGNr(int gNr) {
        this.gNr = gNr;
    }
    public void setBNr(int bNr) {
        this.bNr = bNr;
    }
    public void setBName(String bName) {
        this.bName = bName;
    }
    public void setArea(double area) {
        this.area = area;
    }
    
    /** 
     * @return int kNr
     */
    public int getKNr() {
        return this.kNr;
    }

    
    /** 
     * @return int gNr
     */
    public int getGNr() {
        return this.gNr;
    }

    
    /** 
     * @return int bNr
     */
    public int getBNr() {
        return this.bNr;
    }

    
    /** 
     * @return String bName
     */
    public String getBName() {
        return this.bName;
    }

    
    /** 
     * @return area of the property as a double.
     */
    public double getArea() {
        return this.area;
    }

    /**
     * 
     * @return ID of the property as a String.
     */
    public String propertyID(){
        return kNr + "-" + gNr + "/" + bNr;
    }
        
    /** 
     * @return Property converted to a String.
     */
    public String toString(){
        String output = " -- Eiendom " + propertyID() + " -- \n";
        output += "Eier: " + owner + "\n";
        output += "Kommune: " + kNr + ", " + kName + "\n";
        if(bName != null) output += "Bruksnavn: " + bName + "\n";
        output += "Areal: " + area + "\n";

        return output;
    }
    /**
     * Checks if the property ID of the two Properties matches.
     * @param property prop
     * @return boolean
     */
    public boolean equals(Property property){
        return propertyID().equals(property.propertyID());
    }
}