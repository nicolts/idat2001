package no.ntnu.idatt2001.nicolts;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import no.ntnu.idatt2001.nicolts.ui.controllers.CardGame;

public class Main extends Application {

    @Override
    public void start(Stage stage) {
        Scene scene = new Scene(root(), 900, 720);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    private Parent root() {
        return new CardGame();
    }

    public static void main(String[] args) {
        launch();
    }

}
