package no.ntnu.idatt2001.nicolts.ui.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import no.ntnu.idatt2001.nicolts.core.DeckOfCards;
import no.ntnu.idatt2001.nicolts.core.PlayingCard;
import no.ntnu.idatt2001.nicolts.ui.components.Card;

import java.io.IOException;
import java.util.List;

public class CardGame extends StackPane {
    @FXML private FlowPane cardDisplay;
    @FXML private Label sum;
    @FXML private Label flush;
    @FXML private Label cardsOfHeart;
    @FXML private Label cardsOfSpades;
    @FXML private Label dealHand;
    @FXML private Label checkHand;
    @FXML private Label deckNum;

    private DeckOfCards deck;
    private List<PlayingCard> cardOnDisplay;
    private final Color GREEN = Color.web("#33cc33");
    private final Color RED = Color.web("#ff5050");

    public CardGame() {
        deck = new DeckOfCards();
        init();
        initEventHandlers();
    }

    private void init() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/no/ntnu/idatt2001/nicolts/views/cardgame.fxml"));
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);
            fxmlLoader.load();
        } catch (IOException e ){
            throw new RuntimeException(e);
        }
    }

    private void initEventHandlers() {
        dealHand.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> dealHand());
        checkHand.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> checkHand());
    }

    private void printCard(PlayingCard playingCard) {
        cardDisplay.getChildren().add(new Card(playingCard));
    }

    private void dealHand() {
        cardDisplay.getChildren().clear();
        cardOnDisplay = deck.dealHand(5);
        cardOnDisplay.forEach(this::printCard);
        updateDeckNum();
    }

    private void checkHand() {
        calcFaces();
        calcCardsOfHeart();
        calcCardsOfSpades();
        calcFlush();
    }

    private void updateDeckNum() {
        deckNum.setText(String.valueOf(deck.size()));
    }

    private void calcFaces() {
         int faceSum = cardOnDisplay.stream()
                .mapToInt(PlayingCard::getFace)
                .sum();
        this.sum.setText(String.valueOf(faceSum));
    }

    private void calcFlush() {
        boolean isFlush = cardOnDisplay.stream()
                .map(PlayingCard::getSuit)
                .allMatch(suit -> suit.equals(cardOnDisplay.get(0).getSuit()));

        if (isFlush) {
            flush.setText("Yes");
            flush.setTextFill(GREEN);
            return;
        }
        flush.setText("No");
        flush.setTextFill(RED);
    }

    private void calcCardsOfHeart() {
        StringBuilder sb = new StringBuilder();

        cardOnDisplay.stream()
                .filter(playingCard -> Character.valueOf(playingCard.getSuit()).equals('H'))
                .forEach(playingCard -> sb.append(playingCard.getSuit()).append(playingCard.getFace()).append(" "));

        if (sb.toString().isBlank()) {
            cardsOfHeart.setText("No Cards of Heart");
            cardsOfHeart.setTextFill(RED);
        }

        cardsOfHeart.setText(sb.toString());
        cardsOfHeart.setTextFill(GREEN);
    }

    // I know this is code duplication, I promise I will write generic and reusable code when its being evaluated :D
    private void calcCardsOfSpades() {
        StringBuilder sb = new StringBuilder();

        cardOnDisplay.stream()
                .filter(playingCard -> Character.valueOf(playingCard.getSuit()).equals('S'))
                .forEach(playingCard -> sb.append(playingCard.getSuit()).append(playingCard.getFace()).append(" "));

        if (sb.toString().isBlank()) {
            cardsOfSpades.setText("No Cards of Spades");
            cardsOfSpades.setTextFill(RED);
        }

        cardsOfSpades.setText(sb.toString());
        cardsOfSpades.setTextFill(GREEN);
    }
}
