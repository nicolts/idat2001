package no.ntnu.idatt2001.nicolts.ui.components;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;
import no.ntnu.idatt2001.nicolts.core.PlayingCard;

import java.io.IOException;

public class Card extends StackPane {
    @FXML private Label face;
    @FXML private ImageView suitIconTop;
    @FXML private ImageView suitIconBot;

    public Card(PlayingCard card) {
        init();
        setSuitIconBy(card.getSuit());
        setFace(card.getFace());
        display();
    }

    public void display() {
        this.setOpacity(0);
        this.setTranslateX(-100);
        Timeline timeline = new Timeline();
        KeyFrame keyFrame = new KeyFrame(Duration.millis(500),
                new KeyValue(this.translateXProperty(), 0, Interpolator.EASE_OUT),
                new KeyValue(this.opacityProperty(), 1, Interpolator.EASE_OUT));

        timeline.getKeyFrames().add(keyFrame);
        timeline.play();

    }

    private void init() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/no/ntnu/idatt2001/nicolts/views/card.fxml"));
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);
            fxmlLoader.load();
        } catch (IOException e ){
            throw new RuntimeException(e);
        }
    }

    private void setSuitIconBy(char suit) {
        String imagePath;

        switch (suit) {
            case 'S' -> imagePath = this.getClass().getResource("/no/ntnu/idatt2001/nicolts/img/spade.png").toExternalForm();
            case 'H' -> imagePath = this.getClass().getResource("/no/ntnu/idatt2001/nicolts/img/heart.png").toExternalForm();
            case 'D' -> imagePath = this.getClass().getResource("/no/ntnu/idatt2001/nicolts/img/diamond.png").toExternalForm();
            default -> imagePath = this.getClass().getResource("/no/ntnu/idatt2001/nicolts/img/club.png").toExternalForm();
        }

        setSuitIcon(new Image(imagePath));
    }

    public void setSuitIcon(Image image) {
        suitIconTop.setImage(image);
        suitIconBot.setImage(image);
    }

    public void setFace(int face) {
        String faceDisplay;
        switch (face) {
            case 1 -> faceDisplay = "A";
            case 11 -> faceDisplay = "J";
            case 12 -> faceDisplay = "Q";
            case 13 -> faceDisplay = "K";
            default -> faceDisplay = String.valueOf(face);
        }
        this.face.setText(faceDisplay);
    }

    public Label getFace() {
        return face;
    }
}
