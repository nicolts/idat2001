package no.ntnu.idatt2001.nicolts.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

public class DeckOfCards extends ArrayList<PlayingCard>{
    private final Character[] SUIT = { 'S', 'H', 'D', 'C' };

    public DeckOfCards() {
        super();
        initDeck();
    }

    /**
     * Fills deck.
     */
    public void initDeck() {
        IntStream.range(1, 14)
                .forEach( face -> Arrays.stream( SUIT )
                        .forEach(suit -> add( new PlayingCard(suit, face) )));
    }

    /**
     * Deals a hand with n amount of cards.
     *
     * @param n amount of cards to be dealt.
     * @return ArrayList of cards with size of argument;
     */
    public List<PlayingCard> dealHand(int n) {
        ArrayList<PlayingCard> dealtCards = new ArrayList<>();

        IntStream.range(0, n).forEach(i -> dealtCards.add(dealCard()));
        return dealtCards;
    }

    /**
     * Deals a single card and removes from stock.
     *
     * @return A card
     */
    private PlayingCard dealCard() {
        PlayingCard card;
        Random random = new Random();

        card = get(random.nextInt(size()));
        remove(card);
        return card;
    }
}