module no.ntnu.idatt2001.nicolts {
    requires javafx.controls;
    requires javafx.fxml;

    opens no.ntnu.idatt2001.nicolts to javafx.fxml;
    opens no.ntnu.idatt2001.nicolts.ui.components to javafx.fxml;
    opens no.ntnu.idatt2001.nicolts.ui.controllers to javafx.fxml;


    exports no.ntnu.idatt2001.nicolts.ui.components to javafx.fxml;
    exports no.ntnu.idatt2001.nicolts.core;
    exports no.ntnu.idatt2001.nicolts;
}