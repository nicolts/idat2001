package core;

import no.ntnu.idatt2001.nicolts.core.DeckOfCards;
import no.ntnu.idatt2001.nicolts.core.PlayingCard;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class DeckOfCardsTest {
    private static DeckOfCards deck;

    @BeforeAll
    static void init() {
        deck = new DeckOfCards();
    }

    @Test
    public void correctDeck () {
        // Arrange
        int cardAmount = 0;
        int cardsOfaSuit = 0;

        int correctCardAmount = 52;
        int correctCardsOfaSuit = 13;

        // Act
        cardAmount = deck.size();
        cardsOfaSuit = (int) deck.stream()
                .map(PlayingCard::getSuit)
                .filter(suit -> suit.equals('S'))
                .count();

        // Assert
        assertEquals(cardAmount, correctCardAmount);
        assertEquals(cardsOfaSuit, correctCardsOfaSuit);
    }

    /**
     * Checks id Cards are removed from deck when dealt
     */
    @Test
    public void dealHandFromDeckTest() {
        // Arrange
        int cardAmount;
        int correctCardAmount;
        List<PlayingCard> hand;

        // Act
        correctCardAmount = deck.size() - 5;
        cardAmount = deck.size();
        hand = deck.dealHand(5);

        // Assert
        assertEquals(correctCardAmount, cardAmount);
        deck.forEach(card -> assertFalse(deck.contains(card)));
    }

}
