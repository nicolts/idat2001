package no.ntnu.nicolts.idatt2001.o2.memberships;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MembershipArchiveTest {
    MemberArchive arc = new MemberArchive();

    @Test
    public void registerPointBasicMember() {
        assertTrue(arc.registerPoints(1, 10000));
        assertEquals(arc.findPoints(1, "password"), 20000);
    }

    @Test
    public void registerPointSilverMember() {
        assertTrue(arc.registerPoints(4, 10000));
        assertEquals(arc.findPoints(4, "password"), 42000);
    }

    @Test
    public void registerPointsGoldMember() {
        assertTrue(arc.registerPoints(5, 15000));
        assertEquals(arc.findPoints(5, "password"), 94500);
    }

    @Test
    public void registerNegativePoints() {
        assertTrue(arc.registerPoints(2, -15000));
        assertEquals(arc.findPoints(2, "password"), 15000);
    }

    @Test
    public void findPointsWrongPassword(){
        assertEquals(arc.findPoints(2, "wrong"), -1);
    }

    @Test
    public void findPointsRightPassword(){
        assertEquals(arc.findPoints(2, "password"), 15000);
    }

    @Test
    public void addMemberPositive(){
        assertEquals(arc.addMember(new BonusMember(6, LocalDate.now(), 2000, "je", "je")),6);
    }

    @Test
    public void addMemberWithTakenNumb(){
        assertEquals(arc.addMember(new BonusMember(2, LocalDate.now(), 2000, "je", "je")),-1);
    }
}
