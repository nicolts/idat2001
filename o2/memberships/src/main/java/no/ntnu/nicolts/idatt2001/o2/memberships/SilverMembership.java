package no.ntnu.nicolts.idatt2001.o2.memberships;

public class SilverMembership extends Membership {
    private static final String MEMBERSHIP_NAME = "Silver";
    private static final float POINT_SCALING_FACTOR = 1.2f;

    /**
     * @param bonusPointBalance Amount of points the member has
     * @param newPoints         Points to be added.
     * @return                  New Balance based on membership-type.
     */
    @Override
    public int registerPoints(int bonusPointBalance, int newPoints) {
        float regPoints = (bonusPointBalance + newPoints * POINT_SCALING_FACTOR);
        return Math.round(regPoints);
    }

    /**
     * @return                  Name of the membership.
     */
    @Override
    public String getMembershipName() {
        return MEMBERSHIP_NAME;
    }
}
