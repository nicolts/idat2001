package no.ntnu.nicolts.idatt2001.o2.memberships;

public class GoldMembership extends Membership {
    private static final String MEMBERSHIP_NAME = "Gold";
    private static final float POINT_SCALING_FACTOR_LEVEL_1 = 1.3f;
    private static final float POINT_SCALING_FACTOR_LEVEL_2 = 1.5f;

    /**
     * @param bonusPointBalance Amount of points the member has
     * @param newPoints         Points to be added.
     * @return                  New balance based on membership-type.
     */
    @Override
    public int registerPoints(int bonusPointBalance, int newPoints) {
        float regPoints;

        if (bonusPointBalance >= 90000)
            return Math.round(bonusPointBalance + newPoints * POINT_SCALING_FACTOR_LEVEL_2);
        return Math.round(bonusPointBalance + newPoints * POINT_SCALING_FACTOR_LEVEL_1);
    }

    /**
     * @return                  Name of the membership.
     */
    @Override
    public String getMembershipName() {
        return MEMBERSHIP_NAME;
    }
}
