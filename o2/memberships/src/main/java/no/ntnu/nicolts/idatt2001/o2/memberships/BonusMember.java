package no.ntnu.nicolts.idatt2001.o2.memberships;
import java.time.LocalDate;

public class BonusMember {
    private int memberNumber;
    private LocalDate enrolledDate;
    private int bonusPointsBalance = 0;
    private String name;
    private String emailAdress;
    private String password;
    private Membership membership;
    private static final int SILVER_LIMIT = 25000;
    private static final int GOLD_LIMIT = 75000;

    public BonusMember(int memberNumber, LocalDate enrolledDate, int bonusPointsBalance, String name, String EmailAddress) {
        this.memberNumber = memberNumber;
        this.enrolledDate = enrolledDate;
        this.bonusPointsBalance = bonusPointsBalance;
        this.name = name;
        this.emailAdress = EmailAddress;
        this.password = "password";

        checkAndSetMembership();
    }

    public int registerBonusPoints(int newPoints) {
        if (newPoints <= 0)                     // ingen negative summer.
            return  bonusPointsBalance;
        bonusPointsBalance = membership.registerPoints(bonusPointsBalance, newPoints);
        return bonusPointsBalance;
    }

    public void checkAndSetMembership() {
        if (bonusPointsBalance >= GOLD_LIMIT){
            this.membership = new GoldMembership();
        } else if (bonusPointsBalance >= 25000) {
            this.membership = new SilverMembership();
        } else {
            this.membership = new BasicMembership();
        }
    }

    public boolean checkPassword(String password){
        return this.password.equals(password);
    }

    public int getMemberNumber() {
        return memberNumber;
    }

    public LocalDate getEnrolledDate() {
        return enrolledDate;
    }

    public int getBonusPointsBalance() {
        return bonusPointsBalance;
    }

    public String getName() {
        return name;
    }

    public String getEmailAdress() {
        return emailAdress;
    }

    public String getPassword() {
        return password;
    }

    public Membership getMembership() {
        return membership;
    }

    public static int getSilverLimit() {
        return SILVER_LIMIT;
    }

    public static int getGoldLimit() {
        return GOLD_LIMIT;
    }

    @Override
    public String toString() {
        return  "Name: " + name + "\n" +
                "Member number: " + memberNumber + "\n" +
                "EnrolledDate: " + enrolledDate + "\n" +
                "BonusPointsBalance: " + bonusPointsBalance + "\n" +
                "Emailadress: " + emailAdress + "\n" +
                "Password: " + password + "\n" +
                "Membership: " + membership.getMembershipName() + "\n";
    }
}
