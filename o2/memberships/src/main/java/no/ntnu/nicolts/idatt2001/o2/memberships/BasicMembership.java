package no.ntnu.nicolts.idatt2001.o2.memberships;

/**
 * Basic membership
 */
public class BasicMembership extends Membership {
    private static final String MEMBERSHIP_NAME = "Basic";

    /**
     * @param bonusPointBalance Amount of points the member has
     * @param newPoints         Points to be added.
     * @return                  New balance
     */
    @Override
    public int registerPoints(int bonusPointBalance, int newPoints) {
        return Math.round(bonusPointBalance + newPoints);
    }

    /**
     * @return                  Name of the membership
     */
    @Override
    public String getMembershipName() {
        return MEMBERSHIP_NAME;
    }
}
