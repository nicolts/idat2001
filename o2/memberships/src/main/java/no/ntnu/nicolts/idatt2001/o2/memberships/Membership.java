package no.ntnu.nicolts.idatt2001.o2.memberships;


/**
 * Membership is an abstract class setting the boundaries of the unique membership's.
 */
public abstract class Membership {

    /**
     * registerPoints tar med inn som parameter gjeldende bonuspoeng saldo, og poengene som
     * skal registreres. Metoden beregner så ny saldo basert på fordelene i gitt medlemskap.
     * Den nye saldoen blir så returnert.
     * @param bonusPointBalance Amount of points the member has
     * @param newPoints Points to be added.
     * @return The members new balance post-calculation.
     */
    public abstract int registerPoints(int bonusPointBalance, int newPoints);

    /**
     * getMembershipName returnerer en tekststreng med «navnet» til
     * medlemskapet. Er medlemskapet et basic medlemskap, returneres teksten «Basic», er
     * det sølv, returneres teksten «Silver» osv.
     * @return a String holding the name of the membership.
     */
    public abstract String getMembershipName();
}
