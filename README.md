<h1>Nicolai Sivesinds IDATT2001 Repository</h1>
Repository for obliger, øvinger og andre filer som er relatert til emnet IDATT2001.
<h2>Om Repositoryet</h2>
Hver oblig har en egen mappe som starter med "o" etterfulgt av nummeret til obligen. 
Eksempel:<br>
Oblig 1 --> **o1** <br>
Oblig 2 --> **o2** <br> 

Hver oblig-mappe inneholder prosjekter og filer relatert til obligen.
